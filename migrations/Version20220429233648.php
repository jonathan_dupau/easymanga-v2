<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220429233648 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mangas ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE mangas ADD CONSTRAINT FK_8271C42FA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_8271C42FA76ED395 ON mangas (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mangas DROP FOREIGN KEY FK_8271C42FA76ED395');
        $this->addSql('DROP INDEX IDX_8271C42FA76ED395 ON mangas');
        $this->addSql('ALTER TABLE mangas DROP user_id');
    }
}
