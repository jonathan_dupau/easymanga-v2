<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MangasBySearchController extends AbstractController
{
    #[Route('/mangas/search/{q}', name: 'app_mangas_by_search')]
    public function index(): Response
    {
        return $this->render('mangas_by_search/index.html.twig', [
            'mangas' => $mangas,
        ]);
    }
}
