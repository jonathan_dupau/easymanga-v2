<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\service\CallApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LibraryController extends AbstractController
{
    #[Route('/library/{userId}', name: 'app_library')]
    public function index($userId, UserRepository $userRepository, CallApiService $callApiService): Response
    {
        try {
            $user = $userRepository->findOneBy([
                'id' => $userId,
            ]);
            $library = $user->getMangas()->getValues();
            $mangas = array();
            foreach ($library as $i => $manga) {
                $mangaId = $manga->getMangaId();
                $mangaFromApiData = $callApiService->getMangaById($mangaId);
                if (isset($mangaFromApiData['data'])){
                    $mangaFromApi = $mangaFromApiData['data'];
                    array_push($mangaFromApi, $manga);
                    array_push($mangas, $mangaFromApi);
                }
                if ($i % 2 === 0) {
                    sleep(1);
                }
            }
            return $this->render('library/index.html.twig', [
                'mangas' => $mangas,
            ]);
        }catch (\Exception $e) {
            dd('je comprend pas ce qui se passe sur library controller...');
        }
        $this->addFlash('danger', 'Il y as un soucis avec votre bibliothèque, veuillez contacter un administrateur.');
        return $this->redirectToRoute('app_home');
    }
}
