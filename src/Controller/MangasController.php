<?php

namespace App\Controller;

use App\Entity\Mangas;
use App\Form\SearchMangaByLetterType;
use App\service\CallApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MangasController extends AbstractController
{
    #[Route('/mangas/page/{page}', name: 'app_mangas')]
    public function index($page, CallApiService $callApiService, Request $request): Response
    {
        $mangas = $callApiService->getMangas($page);
        $currentPage = $mangas['pagination']['current_page'];
        $pagination = $mangas['pagination']['last_visible_page'];
        $mangas = $mangas['data'];

        $mangaInstance = new Mangas();

        $searchForm = $this->createForm(SearchMangaByLetterType::class, $mangaInstance);
        $searchForm->handleRequest($request);

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $searchFormData = $searchForm->getData();
            $q = $searchFormData->getName();

            $mangas = $callApiService->getMangaBySearch($q);
            $mangas = $mangas['data'];
            return $this->render('mangas/index.html.twig', [
                'mangas' => $mangas,
                'search_form' => $searchForm->createView(),
            ]);
        }


        return $this->render('mangas/index.html.twig', [
            'mangas' => $mangas,
            'pagination' => $pagination,
            'search_form' => $searchForm->createView(),
        ]);
    }
}
