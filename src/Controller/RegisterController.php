<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterFormType;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use PhpParser\Node\Expr\New_;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends AbstractController
{
    private $entityManager;

    private $userPasswordHasherInterface;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordHasherInterface $userPasswordHasherInterface)
    {
        $this->entityManager = $entityManager;
        $this->userPasswordHasherInterface = $userPasswordHasherInterface;
    }

    #[Route('/inscription', name: 'app_register')]
    public function index(Request $request): Response
    {
        $user = New User();

        $form = $this->createForm(RegisterType::class, $user);
        // récupération du l'objet request au formulaire
        $form->handleRequest($request);

        //  si le formulaire es soumis et validé
        if ($form->isSubmitted() && $form->isValid()) {
            // on récupére la data du form
            $user = $form->getData();
            $password = $this->userPasswordHasherInterface->hashPassword($user, $user->getPassword());
            // // envoi du mot de passe crypter grace au seter
            $user->setPassword($password);

            // ON ENVOIE la data en bdd
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $this->addFlash('success', 'Vous êtes bien inscris, veuillez vous connecter');
            return $this->redirectToRoute('app_login');

        }

        return $this->render('register/index.html.twig', [
            'register_form' => $form->createView()
        ]);
    }
}
