<?php

namespace App\Controller;

use App\Entity\Mangas;
use App\Entity\User;
use App\Form\AddMangaToReadingType;
use App\Form\ChapterReadingType;
use App\Form\RegisterType;
use App\Form\RemoveMangaToReadingType;
use App\Form\SearchMangaByLetterType;
use App\Repository\MangasRepository;
use App\service\CallApiService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MangaController extends AbstractController
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/manga/{id}', name: 'app_manga')]
    public function index($id, CallApiService $callApiService, Request $request, MangasRepository $mangasRepository): Response
    {
        try {
            $mangaData = $callApiService->getMangaById($id);

            if (isset($mangaData['data'])) {
                $mangaData = $mangaData['data'];

                $manga = New Mangas();
                $user = $this->getUser();

                $mangaId = $mangaData['mal_id'];

                if ($user) {
                    $userId = $user->getId();
                    $mangaInUserLibrary = $mangasRepository->findMangaUser($mangaId, $userId);
                    if (isset($mangaInUserLibrary)) {

                        $mangaInUserLibrary = $mangasRepository->findOneById($mangaId);
                        $chapters = $mangaInUserLibrary->getChapters();

                        $removeForm = $this->createForm(RemoveMangaToReadingType::class, $manga);
                        $removeForm->handleRequest($request);

                        if ($removeForm->isSubmitted() && $removeForm->isValid()) {

                            $mangaToDeleteId = $mangaData['mal_id'];
                            $mangaToDelete = $mangasRepository->findOneBy([
                                'manga_id' => $mangaToDeleteId
                            ]);
//                    $manga->setMangaId($mangaDelleted);
//                    $manga->setUser($user);
                            $mangasRepository->remove($mangaToDelete);
                            // ON ENVOIE la data en bdd
//                    $this->entityManager->remove($manga);
//                    $this->entityManager->flush();
                            $this->addFlash('success', 'Manga bien retiré de votre liste !');
                        }

                        $chapterForm = $this->createForm(ChapterReadingType::class, $manga);
                        $chapterForm->handleRequest($request);

                        if ($chapterForm->isSubmitted() && $chapterForm->isValid()) {
                            $chapterData = $chapterForm->getData();
                            $chapter = $chapterData->getChapters();

                            $mangaToEditChapter = $mangasRepository->findOneBy([
                                'manga_id' => $mangaId
                            ]);

                            $mangaToEditChapter->setChapters($chapter);

                            $this->entityManager->flush();
                            $this->addFlash('success', 'Chapitre lu bien edité !');
                        }
                        return $this->render('manga/index.html.twig', [
                            'manga' => $mangaData,
                            'remove_form' => $removeForm->createView(),
                            'chapter_form' => $chapterForm->createView(),
                            'chapters' => $chapters,
                        ]);

                    } else {
                        $addForm = $this->createForm(AddMangaToReadingType::class, $manga);
                        $addForm->handleRequest($request);
                        if ($addForm->isSubmitted() && $addForm->isValid()) {

                            $mangaAdded = $mangaData['mal_id'];

                            $manga->setMangaId($mangaAdded);
                            $manga->setUser($user);

                            // ON ENVOIE la data en bdd
                            $this->entityManager->persist($manga);
                            $this->entityManager->flush();
                            $this->addFlash('success', 'Manga bien ajouté à votre liste !');
                        }
                        return $this->render('manga/index.html.twig', [
                            'manga' => $mangaData,
                            'form' => $addForm->createView(),
                        ]);
                    }
                }
                return $this->render('manga/index.html.twig', [
                    'manga' => $mangaData,
                ]);
            }
        } catch (Exception $e) {
            dd('je comprend pas ce qui se passe sur manga controller...');
        }
        $this->addFlash('danger', 'Il y as un soucis avec ce manga.');
        return $this->redirectToRoute('app_home');
    }
}
