<?php

namespace App\Entity;

use App\Repository\MangasRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MangasRepository::class)]
class Mangas
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $manga_id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'mangas')]
    #[ORM\JoinColumn(nullable: false)]
    private $user;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $name;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $chapters;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getMangaId(): ?int
    {
        return $this->manga_id;
    }

    public function setMangaId(int $manga_id): self
    {
        $this->manga_id = $manga_id;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getChapters(): ?int
    {
        return $this->chapters;
    }

    public function setChapters(?int $chapters): self
    {
        $this->chapters = $chapters;

        return $this;
    }
}
